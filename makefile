build:
	bundle exec jekyll build -d public

open:
	open http://localhost:4000/

run:
	bundle exec jekyll serve --livereload -d public
