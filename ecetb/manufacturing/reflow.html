---
layout: ecetb
title: Reflow Soldering
---
<h1>Reflow Soldering</h1>

<h2>Double-Sided Assembly</h2>
<p>
    Assembling a PCB with surface mount components on both sides of the board can be difficult when reflow 
    soldering. One side of the board must be facing down, and so the components mounted to the bottom-facing 
    side of the board may fall once their solder joints melt. Unless all components on the bottom-facing side 
    of the PCB are glued to the PCB, components may only be reflow soldered when their side of the PCB is 
    facing upwards. This leads to the typical requirement that double-sided PCBAs must go through two separate 
    reflow cycles.
</p>
<p>
    Fortunately, the surface tension of molten solder is significant. If a component has a total wetted pad 
    surface area great enough to counteract the effect of gravity (and potential mechanical shock imparted when 
    travelling through a mechanised reflow oven) on the mass of the component, the component will not fall. As 
    a general rule, it may be assumed that components which meet the following metric will not fall:
</p>
<div class="eq">
    $$\begin{equation}
    {46 \frac{mg}{mm^2} > \frac{M_{component}}{A_{contact}}}
    \label{eq:component_mass_limit}
    \end{equation}$$
</div>
<p>
    But what if some components do not meet the aforementioned metric, and do not provide enough pad area? The 
    general strategy is to try to have all of these heavy components on one side of the PCB, along with 
    temperature-sensitive components would preferably only be reflowed once (such as some precision analog 
    parts and non-volatile memories). This side will be referenced to as the "primary side". The secondary side 
    must solely be populated with components that are neither too heavy nor temperature-sensitive. The 
    following procedure may be followed:
</p>
<ol>
    <li>Apply solder paste to the secondary side</li>
    <li>Place components on the secondary side</li>
    <li>Reflow the partially-populated PCBA with the secondary side facing upwards</li>
    <li>
        Clean the partially-populated PCBA once it has cooled. This step is optional depending on the flux 
        used. Flux will generally become harder to remove with every reflow.
    </li>
    <li>
        Apply solder paste to the primary side. The PCB will likely need to be elevated off of a surface due to 
        the uneven height of components now on the bottom of the PCB
    </li>
    <li>Place components on the primary side</li>
    <li>
        Reflow the PCBA with the primary side facing upwards. The PCB will need to be elevated, as any pressure 
        on the secondary side components will result in a crooked or even desoldered component
    </li>
    <li>Clean the PCBA once it has cooled</li>
</ol>

<h2>Package Acceptability for Bottom-Mounting</h2>
<p>
    The following tables list the component packages that should be able to be bottom-mounted without 
    additional adhesives needing to be applied.
</p>

<h3>Capacitors</h3>
<table>
    <tr>
        <th>Package</th>
        <th>Typical mass (mg)</th>
        <th>Typical contact area (mm<sup>2</sup>)</th>
        <th>Typical ratio (mg/mm<sup>2</sup>)</th>
        <th>Acceptability</th>
    </tr>
    <tr>
        <td>0201</td> <!-- Package -->
        <td>0.57</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>0402</td> <!-- Package -->
        <td>3.10</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>0603</td> <!-- Package -->
        <td>9.10</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>0805</td> <!-- Package -->
        <td>23.0</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>1206</td> <!-- Package -->
        <td>64.0</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>1210</td> <!-- Package -->
        <td>110</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
</table>

<h3>Resistors</h3>
<table>
    <tr>
        <th>Package</th>
        <th>Typical mass (mg)</th>
        <th>Typical contact area (mm<sup>2</sup>)</th>
        <th>Typical ratio (mg/mm<sup>2</sup>)</th>
        <th>Acceptability</th>
    </tr>
    <tr>
        <td>0201</td> <!-- Package -->
        <td>0.17</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>0402</td> <!-- Package -->
        <td>0.65</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>0603</td> <!-- Package -->
        <td>2.00</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>0805</td> <!-- Package -->
        <td>5.5</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>1206</td> <!-- Package -->
        <td>10.0</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>1210</td> <!-- Package -->
        <td>16.0</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
    <tr>
        <td>2010</td> <!-- Package -->
        <td>25.5</td> <!-- Typical mass -->
        <td></td> <!-- Typical contact area -->
        <td></td> <!-- Typical ratio -->
        <td>Yes</td> <!-- Acceptability -->
    </tr>
</table>
