[![Build Status](https://gitlab.com/pages/jekyll/badges/master/pipeline.svg)](https://gitlab.com/pages/jekyll/-/pipelines?ref=master)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

DJFL website repository.

## Organisation
- Blog posts go in /_posts.
- Draft pages go in /_drafts.
- ECE Toolbox pages go in /ecetb.
- Images go in /images. Most images are page-specific so they should be put in a subfolder named the same as the page file that they are included in. Site-wide images are put in /images.

## Development
This website uses Jekyll as a static site generator. In order to develop the website:
1. Clone this repository
2. Install Jekyll
3. Make the changes required
4. Run the following command to compile and start the local server
```
make run
```
5. Open Run the following command to http://localhost:4000/ or run the following command
```
make open
```
